


<?php
session_start();
//ini_set( 'display_errors', 1 );
# Set configlation
include_once "config.php";
// $url,$sslUrl,$ref
# Set navigation
//include_once "navigation.php";
// top,normal
# Set analytics
//include_once "analyticstracking.php";
// googleAnalytics,facebookAnalytics,googleRemarketing,ymdRemarketing,facebook
# Set rate needs to top page only
include_once "global_rate.php";
//USD,JPY,XRP
?>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="">
<meta name="author" content="">

<title>AIC ESTONIA</title>

<!-- Fonts -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
<link rel="stylesheet" href="assets/fonts/novecento/stylesheet.css">
<link rel="stylesheet" href="assets/icons/glyphicons/style.min.css">
<link rel="stylesheet" href="assets/icons/font-awesome/font-awesome.min.css">

<!-- Styles -->
<!--CHANGCE COLOR sreyneang test color -->
<link rel="stylesheet" href="assets/css/style2.css">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/style.min.css">
<link rel="stylesheet" href="assets/css/theme-light.css">
<!--<link rel="stylesheet" href="assets/css/theme-dark.css">-->
<link rel="stylesheet" href="css/mystyle.css">
<link rel="stylesheet" href="css/degicoin.css">

<!-- GDPR -->
<link rel="stylesheet" href="assets/css/jquery.cookieBar.css">


<!-- Plugins -->
<link rel="stylesheet" href="assets/plugins/royalslider/royalslider.min.css">
<link rel="stylesheet" href="assets/plugins/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" href="assets/plugins/mfp/jquery.mfp.css">

<!-- Additional styles -->
<style>
#culture {
	background-image: url(demo/img/culture-bg.jpg);
}
</style>

</head>

<body>

<div class="site-loader"></div>

<section id="home" class="site-splash fullscreen">

	<div class="splash-inner">

		<div class="splash-slider">

			<div class="splash-content"><div class="container">

					<div class="row">

						<div class="col-md-12">

							<div class="splash-text">
								<h1 class="splash-headline">WE ARE <span class="fontc-degicoin">AIC</span>ESTONIA</h1>
								<div class="splash-description">
									<p><span class="fontb-normal">every country every use</span></p>
								</div>
							</div>

						</div>

					</div>

			</div></div>

			<div class="splash-content"><div class="container">

					<div class="row">

						<div class="col-md-12">

							<div class="splash-text">
								<h1 class="splash-headline">WE <span class="fontc-degicoin">NEED</span> INNOVATION!!</h1>
								<div class="splash-description">
								</div>
							</div>

						</div>

					</div>

			</div></div>

			<div class="splash-content"><div class="container">

					<div class="row">

						<div class="col-md-12">

							<div class="splash-text">
								<h1 class="splash-headline">MAKE <span class="fontc-degicoin">THE</span> IMPOSSIBLE POSSIBLE</h1>
								<div class="splash-description">
									<p>Anything one man can imagine, other men can make real</p>
								</div>
							</div>

						</div>

					</div>

			</div></div>

			<!--<div class="cycle-next"></div>-->
			<!--<div class="cycle-prev"></div>-->

		</div>

		<div class="splash-media splash-media-img" 
			data-background="demo/img/splash-bg.jpg" data-background-xs="demo/img/splash-bg-xs.jpg" data-background-md="demo/img/splash-bg-md.jpg" data-background-lg="demo/img/splash-bg-lg.jpg">
			<div class="overlay"></div>
		</div>

		<div class="splash-feedback">
			<span class="mouse">
				<span class="fa fa-angle-down"></span>
			</span>
			<span class="caption">Scroll</span>
		</div>

	</div>

</section>

<header class="site-header th-dark">

	<div class="header-inner">

		<div class="container">

			<div class="row">

				<div class="header-table col-md-12">

					<div class="brand">
						<a href="#">
							<img src="img/degicoin_title.png" alt="AIC">
						</a>
					</div>

					<nav class="main-nav">
						<a href="#" class="nav-toggle"></a>
						<ul class="nav">
							<!-- Menu items here -->
							<li><a href="#">Home</a></li>
							<!--<li><a href="http://fujicrypto.com/session.html">Session</a></li>-->
							<!--<li><a href="#about">About</a></li>-->
							<li>
								<a href="#about">
									About Menu
									<span class="sub-toggle"></span>
								</a>
								<ul>
									<li><a href="#about">About</a></li>
									<li><a href="<?= $url ?>corp.php">Corporation</a></li>
									<li><a href="<?= $url ?>seminar.php">Seminar</a></li>
									<li><a href="<?= $url ?>news.php">News</a></li>
									<li><a href="<?= $url ?>attention.php">Attention</a></li>
								</ul>
							</li>
							
							<li>
								<a href="#currency">
									Currency Menu
									<span class="sub-toggle"></span>
								</a>
								<ul>
									<li><a href="#currency">Currency</a></li>
									<!--
									<li><a href="http://gdk.fujicrypto.com/">goldkey</a></li>
									-->
									<!--
									<li><a href="bitcoin.html">bitcoin</a></li>
									<li><a href="ripple.html">ripple</a></li>
									-->
								</ul>
							</li>
							<li><a href="<?= $url ?>exchang.php">Exchange</a></li>
							<!--<li><a href="#currency">Currency</a></li>-->
							<!--<li><a href="#guide">Guide</a></li>-->
							<li>
								<a href="#guide">
									Guide Menu
									<span class="sub-toggle"></span>
								</a>
								<ul>
									<li><a href="#guide">Guide</a></li>
									<li><a href="<?= $url ?>uservideo.php">Video</a></li>
								</ul>
							</li>
							<li><a href="#purchase">Order
									<span class="sub-toggle"></span>
								</a>
								<!--
								<ul>
									<li><a href="<?= $sslUrl ?>sctl.php">Specified Commercial Transactions</a></li>
								</ul>
								-->
							</li>
							<li><a href="<?= $sslUrl ?>contact_input.php">Contact</a></li>

							<!-- transtator language -->

							<li><a href="index.php?hl=en">Khmer Language</a></li>
							<!--<li><a href="https://fujicrypto.com/pamphlet_input.html">Pamphlet</a></li>-->
							<!--
							<li>
								<a href="#">
									Multilevel Menu
									<span class="sub-toggle"></span>
								</a>
								<ul>
									<li><a href="#">Multilevel Menu</a></li>
									<li><a href="#">Multilevel Menu</a></li>
								</ul>
							</li>
							-->
						</ul>
					</nav>

				</div>

			</div>

		</div>

	</div>

</header>

<div class="site-wrapper">
	
	<div class="site-body">

		<section id="about" class="section th-dark">

			<div class="section-row-container">

				<div class="section-row">

					<div class="container">

						<div class="row">

							<div class="col-lg-3">

								<h1 class="section-title show-counter">
									THE RATE
									<small>Rate is updated when you update this screen</small>
								</h1>

							</div>

							<div class="col-lg-8 col-lg-push-1">
								<div class="row">
									<div class="col-md-12">
										<p>
				
						 					Rate is in doller<!--当サイト<span class="fontc-degicoin">Fujicrypto</span>で購入代行を希望される方はお気軽にご質問ください。--><br>
											<span class="fontc-degicoin">※This price is not the current exchange rate. The exchange rate is shown on the exchange page.</span>
										</p>
									</div>
								</div>

								<div class="row">
									<div class="col-md-4">
										<div class="icon-box" data-animation-name="fadeInUp">
											<div class="text-center">
												<div class="custom_number"><!--Ask：&yen;--><?php print $USDBTC; ?> <span class="fonts-30 fontb-normal">$</span></div>
												<!--<div class="custom_number">Bid：&yen;<?php print $BTCUSD_bid; ?></div>-->
												<p>Bitcoin</p>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="icon-box" data-animation-name="fadeInUp">
											<div class="text-center">
												<div class="custom_number"><?php print $USDXRP; ?> <span class="fonts-30 fontb-normal">$</span></div>
												<!--<div class="custom_number"><--Ask：&yen;--<--<?php print $XRPUSD_bid; ?>--<span id="xrprate-dis"></span> <span class="fonts-30 fontb-normal">円</span></div>-->
												<!--<div class="custom_number">Bid：　　－　　!--<?php print $XRPUSD_ask; ?>--</div>-->
												<p>Ripple</p>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="icon-box" data-animation-name="fadeInUp">
											<div class="text-center">
												<div class="custom_number"><?php print $USDETH; ?> <span class="fonts-30 fontb-normal">$</span></div>
												<!--<div class="custom_number"><--Ask：&yen;--<--<?php print $XRPUSD_bid; ?>--<span id="xrprate-dis"></span> <span class="fonts-30 fontb-normal">円</span></div>-->
												<!--<div class="custom_number">Bid：　　－　　!--<?php print $XRPUSD_ask; ?>--</div>-->
												<p>Ethereum</p>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<p>
				
						 					Rate is in EUR<!--当サイト<span class="fontc-degicoin">Fujicrypto</span>で購入代行を希望される方はお気軽にご質問ください。--><br>
											<span class="fontc-degicoin">※This price is not the current exchange rate. The exchange rate is shown on the exchange page.</span>
										</p>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="icon-box" data-animation-name="fadeInUp">
											<div class="text-center">
												<div class="custom_number"><!--Ask：&yen;--><?php print $EURUSD; ?> <span class="fonts-30 fontb-normal">€</span></div>
												<!--<div class="custom_number">Bid：&yen;<?php print $USDJPY_bid; ?></div>-->
												<p>USDEUR</p>
											</div>
											<!--
											<div class="counter" data-animation-name="fadeInUp">
												<span class="number">102．09</span>
												<span class="label">USDJPY</span>
											</div>
											-->
										</div>
									</div>
									<div class="col-md-6">
										<div class="icon-box" data-animation-name="fadeInUp">
											<div class="text-center">
												<div class="custom_number"><!--Ask：&yen;--><?php print $EURBTC; ?> <span class="fonts-30 fontb-normal">€</span></div>
												<!--<div class="custom_number">Bid：&yen;<?php print $BTCUSD_bid; ?></div>-->
												<p>Bitcoin</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="icon-box" data-animation-name="fadeInUp">
											<div class="text-center">
												<div class="custom_number"><?php print $EURXRP; ?> <span class="fonts-30 fontb-normal">€</span></div>
												<!--<div class="custom_number"><--Ask：&yen;--<--<?php print $XRPUSD_bid; ?>--<span id="xrprate-dis"></span> <span class="fonts-30 fontb-normal">円</span></div>-->
												<!--<div class="custom_number">Bid：　　－　　!--<?php print $XRPUSD_ask; ?>--</div>-->
												<p>Ripple</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="icon-box" data-animation-name="fadeInUp">
											<div class="text-center">
												<div class="custom_number"><?php print $EURETH; ?> <span class="fonts-30 fontb-normal">€</span></div>
												<!--<div class="custom_number"><--Ask：&yen;--<--<?php print $XRPUSD_bid; ?>--<span id="xrprate-dis"></span> <span class="fonts-30 fontb-normal">円</span></div>-->
												<!--<div class="custom_number">Bid：　　－　　!--<?php print $XRPUSD_ask; ?>--</div>-->
												<p>Ethereum</p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<small>※Display price rate are changing when browser update</small><br>
										<!--<small>※参考レート　この価格で購入出来る訳ではありません</small><br>
										<small>※表示価格に手数料を加算した金額での販売となります。</small><br>-->
										
										<div class="mtop20">
											<div class="fb-like" data-href="https://www.facebook.com/fujicrypto?ref=aymt_homepage_panel" data-width="500" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
										</div>
									</div>
								</div>
								
								
								<div class="row">
									<div class="col-md-12">
										<h4 class="section-separator-title mtop50">
											<span data-shadow-text=" INFORMATION" class="fontc-blue">INFORMATION</span>
										</h4>
<!--

										<ul class="list-unstyled mtop30">
										
										<li class="padding5 text10 boder-solid"><a href="http://gigaom.com/2013/05/14/google-ventures-invests-in-opencoin-the-firm-behind-bitcoin-exchange-ripple/" target="_blank">2013-05-14　<span class="fontc-white bgc-black padding10"><i class="gi gi-share"></i></span>　Googleベンチャーズは、リップルラボへの投資を正式に決定</a></li>
										<li class="padding5 mtop10 text10 boder-solid"><a href="http://jp.wsj.com/news/articles/SB10001424052702303572004579448492031921668?tesla=y" target="_blank">2014-03-20　<span class="fontc-white bgc-black padding10"><i class="gi gi-tag"></i></span>　米ヘッジファンド会社がビットコインに特化したファンド創設へ</a></li>
										<li class="padding5 mtop10 text10 boder-solid"><a href="http://business.nikkeibp.co.jp/article/report/20140421/263232/?ST=tech&rt=nocnt" target="_blank">2014-04-22　<span class="fontc-white bgc-black padding10"><i class="gi gi-paperclip"></i></span>　日経ビジネス　：　「リップル」はデジタル通貨の本命か</a></li>
										<li class="padding5 mtop10 text10 boder-solid"><a href="https://ripple.com/blog/fidor-bank-ag-the-first-bank-to-use-the-ripple-protocol/" target="_blank">2014-05-03　<span class="fontc-white bgc-black padding10"><i class="gi gi-gbp"></i></span>　ドイツの国際銀行Fidor銀行がRippleシステムを導入</a></li>
										<li class="padding5 mtop10 text10 boder-solid">2014-05-12　<span class="fontc-white bgc-black padding10"><i class="gi gi-home"></i></span>　ホームページを更新致しました</li>
										<li class="padding5 mtop10 text10 boder-solid"><a href="http://techwave.jp/archives/robocoin_will_appear_at_nishiazabu.html" target="_blank">2014-05-12　<span class="fontc-white bgc-black padding10"><i class="gi gi-pushpin"></i></span>　日本初となるビットコインATM「ROBOCOIN（ロボコイン）」が東京西麻布に登場予定 </a></li>
										<li class="padding5 mtop10 text10 boder-solid"><a href="http://www.bloomberg.co.jp/news/123-N5JJSS6TTDS701.html" target="_blank">2014-05-13　<span class="fontc-white bgc-black padding10"><i class="gi gi-usd"></i></span>　ビットコイン決済サービスのビットペイ、3000万ドル出資確保 </a></li>
										<li class="padding5 mtop10 text10 boder-solid"><a href="http://www.47news.jp/CN/201405/CN2014051301002271.html" target="_blank">2014-05-03　<span class="fontc-white bgc-black padding10"><i class="gi gi-check"></i></span>　ビットコイン規制見送り</a></li>

										</ul>
-->

<style>
.news-day{
	width:70px;
}
.news-btc{
	width:50px;
	background-image:url("img/news-btc.png");
	background-size:contain;
	background-repeat: no-repeat;
}
.news-xrp{
	width:50px;
	background-image:url("img/news-xrp.png");
	background-size:contain;
	background-repeat: no-repeat;
}
.news-eth{
	width:50px;
	background-image:url("img/news-eth.png");
	background-size:contain;
	background-repeat: no-repeat;
}
.news-xnf{
	width:50px;
	background-image:url("img/news-xnf.png");
	background-size:contain;
	background-repeat: no-repeat;
}
.news-fjc{
	width:50px;
	background-image:url("img/news-fjc.png");
	background-size:contain;
	background-repeat: no-repeat;
}
.news-card{
	width:50px;
	background-image:url("img/card.png");
	background-size:contain;
	background-repeat: no-repeat;
}
.news-bitred{
	width:50px;
	background-image:url("img/bitred.png");
	background-size:contain;
	background-repeat: no-repeat;
}
.news-attention{
	width:50px;
	background-image:url("img/news-attention.png");
	background-size:contain;
	background-repeat: no-repeat;
}
</style>
<?php
/*
									<!--ここにニュースを記入してください---------------------------------->

										<!--<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2015/08/04</td>
											<td class="news-card"></td>
											<td><a href="<?= $url ?>card.html" target="_blank"><span class="fontc-blue">ANX プレミアムビットコインデビットカード誕生！　受付開始！！</span> <span class="fontc-white bgc-red padding5 fonts-80">Check!</span></a></td>
											</tr>
										</table>-->
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2016/12/25</td>
											<td class="news-fjc"></td>
											<td><a href="https://www.nta.go.jp/shiraberu/zeiho-kaishaku/joho-zeikaishaku/shotoku/shinkoku/171127/01.pdf"><span class="fontc-blue">弊社の年末年始の休業は、2017年12月28日〜2018年1月8日迄となります。</span> <span class="fontc-white bgc-red padding5 fonts-80">Check!</span></a></td>
											</tr>
										</table>
									   	<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2016/12/01</td>
											<td class="news-fjc"></td>
											<td><a href="https://www.nta.go.jp/shiraberu/zeiho-kaishaku/joho-zeikaishaku/shotoku/shinkoku/171127/01.pdf"><span class="fontc-blue">国税庁の仮想通貨取引に関するFAQ</span> <span class="fontc-white bgc-red padding5 fonts-80">Check!</span></a></td>
											</tr>
										</table>
										
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2017/10/01</td>
											<td class="news-fjc"></td>
											<td><span class="fontc-blue">
【仮想通貨交換業の廃業について】<br>
　<p>当社は、平成29年9月30日をもって、仮想通貨交換業を廃止いたしました。</p>
<p>資金決済に関する法律第六十三条の二十第五項に規定する仮想通貨交換業として行う仮想通貨の交換等に関し負担する債務の履行及び当該仮想通貨交換業に関し管理する利用者の財産の返還又は利用者への移転の方法につきましては、以下のとおりといたします。</p>

<p>一　当社が管理する利用者の仮想通貨については、当社が認める時期までに、該当する仮想通貨の保管が可能な利用者が管理するウォレットに送付する方法により返還いたします。</p>

<p>二　前記一の方法による返還が困難な場合には、保管する仮想通貨を日本円に換価し日本円にて利用者の指定の銀行口座に振り込む方法にて返還いたします。</p>

<p>以上、資金決済に関する法律第六十三条の二十第三項の規定により公告いたします。<br>
平成29年11月21日</p>

　<p>東京都港区麻布十番２−５−２　JMNビル３階<br>
　フジクリプト株式会社<br>
　代表取締役　綾野秀紀</p>

</span> <span class="fontc-white bgc-red padding5 fonts-80">Check!</span></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2016/08/27</td>
											<td class="news-fjc"></td>
											<td><a href="<?= $url ?>exchang.php"><span class="fontc-blue">Money Exchange!!　（両替所始めました。店頭で各国の通貨を円に）</span> <span class="fontc-white bgc-red padding5 fonts-80">Check!</span></a></td>
											</tr>
										</table>	
																											
			
										<h4 class="section-separator-title mtop50">
											<span data-shadow-text=" DONATE">DONATE</span>
										</h4>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2016/04/19</td>
											<td class="news-bitred"></td>
											<td><a href="<?= $url ?>contribution.php">熊本地震義援金へのご協力ありがとうございました。
											<br>お寄せいただいた義援金は、日本赤十字社へ全額寄付させていただきました。</a></td>
											</tr>
										</table>	
										
										
                                    	<h4 class="section-separator-title mtop50">
											<span data-shadow-text=" PICKUP">PICKUP</span>
										</h4>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2018/7/23</td>
											<td class="news-eth"></td>
											<td><a href="http://www.builduptrust.com/wp/2018/05/31/washington/">スティーブバノン氏というトランプ大統領の元アドバイザーが仮想通貨を作っています</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2018/7/13</td>
											<td class="news-xrp"></td>
											<td><a href="http://www.builduptrust.com/wp/2018/07/13/ripple-wants/">リップル社はインドでビットコインを打ち負かしたい</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2018/07/6</td>
											<td class="news-btc"></td>
											<td><a href="http://www.builduptrust.com/wp/2018/06/26/korean-crypto/">国際決済銀行の課長は「お金の創造を辞めて！</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2018/06/29</td>
											<td class="news-btc"></td>
											<td><a href="http://www.builduptrust.com/wp/2018/06/26/korean-crypto/">韓国の取引所がユーザのプライベートキーを公開して 650,000ドルを危険にします</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2018/06/26</td>
											<td class="news-btc"></td>
											<td><a href="http://www.builduptrust.com/wp/2018/06/21/amsterdams/">アムステルダムの空港は、旅行者が余分なユーロをビットコイン・イーサリアムに交換できるようにします。</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2018/06/15</td>
											<td class="news-btc"></td>
											<td><a href="http://www.builduptrust.com/wp/2018/06/15/sec-announces/">SECは仮想通貨が証券ではないことを発表します</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2018/06/08</td>
											<td class="news-btc"></td>
											<td><a href="http://www.builduptrust.com/wp/2018/06/04/coinbase-2/" target="_blank">Coinbase社は日本に来て、なお・きたざわ氏のフィンテックのリーダーを歓迎します</a></td>
											</tr>
										</table>
										
										
										
										
										
										
										
										
										
										
										
										

										
										
										
										
										
										
										
										
										
										
																				
										
										
										
										
										
										
										
										
										 
										
										
										 
										
											
								
									    
								
									
						
									 
										
										
								
								
									
										
										
							
									
										
										


										<h4 class="section-separator-title mtop50">
											<span data-shadow-text=" ATTENTION" class="fontc-red">ATTENTION</span>
										</h4>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2017/10/01</td>
											<td class="news-attention"></td>
											<td><span class="fontc-red">＜仮想通貨の売買は現在お取り扱いしておりません＞<br>Note: We are currently in the process of applying for a cryptocurrency trading license. However, until sucessful, we can not buy or sell bitcoin at the moment. If you have any further question, please call us at (050) 3803 8877 or email.</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2017/09/11</td>
											<td class="news-attention"></td>
											<td><span class="fontc-red">MyEtherWalletのフィッシングに注意して下さい！プライベートキーが盗まれてしまいます！「myctherwallet.com/」フィッシングサイトはEがCになっています。</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2016/04/01</td>
											<td class="news-attention"></td>
											<td><span class="fontc-red">GateHubへの移行サポート業務を終了致しました。</a></td>
											</tr>
										</table>
										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2016/03/24</td>
											<td class="news-attention"></td>
											<td><span class="fontc-red">偽のメールにご注意ください！「gateshub.net」「ripplexlabs.com」などから来たメールは開かずに削除して下さい。</a></td>
											</tr>
										</table>
										

										<table class="table boder-solid mbottom10">
											<tr>
											<td class="news-day">2014/10/06</td>
											<td class="news-attention"></td>
											<td><a href="<?= $url ?>attention.php" target="_blank"><span class="fontc-red">フジクリプトの名を騙った社債・株式・デジタル通貨等への投資勧誘詐欺にご注意ください</span></a></td>
											</tr>
										</table>
*/
?>
									</div>
								</div>
								
								
							</div>

						</div>

					</div>

				</div>

				<div class="section-row no-padding-top">

					<div class="container">

						<div class="row">

							<div class="col-md-8 col-md-push-4">

								<h2 class="section-separator-title">
									<span data-shadow-text=" ABOUT AIC">ABOUT AIC</span>
								</h2>

							</div>

						</div>

						<div class="row">

							<div class="col-md-8 col-md-push-4">
								<p>
									<span class="fontc-degicoin">AIC</span>will be feuture company what new currency exchange system. Using bitcoin ripple several crypto currency are expand global.
								</p>
								<p class="mtop20">
									The settlement system of the 21st century has come at a time of dramatic change. In today's information-oriented speed society, paying thousands of yen for overseas remittances, it will be out of date that it will take days to arrive. If you use this digital currency for settlement, you will be able to do "low cost" further and "instantly" to countries that are separate from Japan.
								</p>
								<p class="mtop20">
									Through this site, we will let many people know the structure and products of digital currency, and we will work to make transactions easily in Global.
								</p>
							</div>

						</div>

						<div class="section-row-container">
							<div class="section-row pbottom10 project-title">
								<div class="container">
									<!--<h2 class="section-separator-title mtop10">
													<span data-shadow-text="MEET OUR TEAM">MEET OUR TEAM</span>
												</h2>
									<div class="row">
										<div class="col-md-3 col-sm-6">
											<div class="team" data-animation-name="fadeInRight">
												<div class="team-photo">
													<img src="img/stf_01.png" />
												</div>
												<div class="team-info">
													<h4 class="team-name">綾野　秀紀</h4>
												</div>
												<div class="team-info">
													<medium>CEO, Creative Director</medium>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-6">
											<div class="team" data-animation-name="fadeInRight">
												<div class="team-photo">
													<img src="img/stf_02.png" />				
												</div>
												<div class="team-info">
													<h4 class="team-name">牧野　俊介</h4>
												</div>
												<div class="team-info">
													<medium>Business Manager</medium>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-6">
											<div class="team" data-animation-name="fadeInRight">
												<div class="team-photo">
													<img src="img/stf_03.png" />
												</div>
												<div class="team-info">
													<h4 class="team-name">トリゾリーノ　サラ</h4>
												</div>
												<div class="team-info">
													<medium>Secretary</medium>
												</div>
											</div>
										</div>
										<div class="col-md-3 col-sm-6">
											<div class="team" data-animation-name="fadeInRight">
												<div class="team-photo">
													<img src="img/stf_04.png" />					
												</div>
												<div class="team-info">
													<h4 class="team-name">中村　壮志</h4>
												</div>
												<div class="team-info">
													<medium>Crypto Currency Adviser</medium>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3 col-sm-6">
											<div class="team" data-animation-name="fadeInRight">
											</div>
										</div>
										<div class="col-md-3 col-sm-6">
											<div class="team" data-animation-name="fadeInRight">
											</div>
										</div>
										<div class="col-md-3 col-sm-6">
											<div class="team" data-animation-name="fadeInRight">
											</div>
										</div>
										<div class="col-md-3 col-sm-6">
											<div class="team" data-animation-name="fadeInRight">
												<div class="team-info">
													<medium>designed by freepik.com</medium>
												</div>
											</div>
										</div>
									</div>-->
									<div class="row">

							<div class="col-md-12">
								<h4>Corporate philosophy</h4>
								<p class="lead">Contribute to Estonia Society ⇔ Global Can not Defeat to Companies around the World</p>
								<p>We are convinced that products such as digital currency deeply penetrate our daily lives, such as the Internet system, making society convenient and "one for each family and one for each person".</p>
								<p>The Internet made it possible to provide and contact information "instantly" to people living in any country of the world. And the settlement system in digital currency is trying to make it possible to send asset value instantaneously to anywhere in the world in the same way.</p>
								<p>By combining the Internet and the digital currency, everyone in any country or individual in any country can develop "fair" business development and information to all over the world by combining the Internet and the digital currency about it.</p>
								<p>Digital currencies are already penetrating society in countries all over the world (especially the United States).</p>
								<p>We will prepare Estonia infrastructure in this digital currency and aim to become a global company that can be a bridge from Estonia to the world and from the world to Japan.</p>
								<p>Through this site, I would like to contribute even a little so that many people will know the charm of the digital currency and the mechanism of the future payment system and make it a convenient world.</p>
							</div>

						</div>
								</div>
							</div>
						</div>

					</div>

				</div>

				

				

			</div>

		</section>

		<section id="testimonials2" class="section th-dark" 
			data-background="demo/parallax/1.jpg" data-background-xs="demo/parallax/1-xs.jpg" data-background-md="demo/parallax/1-md.jpg" data-background-lg="demo/parallax/1-lg.jpg">

			<div class="section-row-container">

				<div class="section-row three-quarters-padding-top three-quarters-padding-bottom">

					<div class="container">

						<div class="row">

							<div class="col-md-10 col-md-push-1">

								<div class="testimonial-slider owl-carousel" data-auto-height="true" data-single-item="true" data-animation-name="rotateInUpLeft">

									<div class="testimonial">
										<blockquote>
											<p class ="fontc-white">Good products change the all!</p>
											<div><small><span class ="fontc-white">Digital currency is a new payment system.</span><br/><br/></small></div><!--
											<p class ="fontc-white"><a href="https://www.ripple.com/" target="_blank">Ripple</a></p>
													<p class ="fontc-white"><a href="https://blockchain.info/ja" target="_blank">BLOCKCHAIN</a></p>-->
										</blockquote>
									</div>
<!--
									<div class="testimonial">
										<blockquote>
											<p>Hydrogen is an awesome company!</p>
											<small>Dennis Nedry <cite>Jurassic Park</cite></small>
										</blockquote>
									</div>

									<div class="testimonial">
										<blockquote>
											<p>Awesome support, awesome work and great design! We couldn't ask for more from the great folks at Hydrogen.</p>
											<small>John Weasley <cite>The Burrow</cite></small>
										</blockquote>
									</div>
-->
								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="section-overlay"></div>

		</section>

		<section id="currency" class="section">

			<div class="section-row-container">

				<div class="section-row">

					<div class="container">

						<div class="row">

							<div class="col-lg-12">

								<h1 class="section-title show-counter">
									VIRTUAL CURRENCY
									<small>Currency that can be remitted easily to anywhere in the world with a very low commission</small>
								</h1>

							</div>

						</div>


						<div class="row">

							<div class="col-md-6">

								<div class="service">
									<h4 class="service-name"><img src="img/bitcoin_logo.png" ></h4>
									<div class="service-info mleft50">
										<p class="service-description">
											Bitcoin was born in 2009 "encrypted digital currency" on the Internet
It is what is said.
Transaction costs are suppressed because central agencies do not exist, and transactions are said to be possible by waiting on average about 10 minutes from remittance.
										</p>
										<p class="service-description mtop20">
											Currently it can be used in various online shops, etc. It is attractive to keep expenses lower than the cost of common credit cards.
										</p>
										<p class="service-description mtop20">
											Because the amount of production is predetermined, the rarity value increases as the number of users increases, and we can expect the price to rise.
Overseas you can do bitcoin settlement in various places. For example, real estate, cars, electric appliances and so on.


										</p>
										<p class="service-description mtop20">
											However, the history of Bitcoin is shallow, exceeding 1 dollar for the first time in February 2011, exceeding $ 1,200 at the beginning of October 2013. About two thousand times the threat in two years! !
										</p>
<p class="service-description mtop20">
											The price which was overheated now has calmed down, but there seem to be some people who are watching whether it will rise further. (It is over $ 10,000 as of 2017)
										</p>
									</div>

								</div>

							</div>

							<div class="col-md-6">
								<div class="service">
									<h4 class="service-name"><img src="img/ripple_logo.png" ></h4>
									<div class="service-info mleft50">
										<p class="service-description">
											Ripple is "encrypted digital transaction network" born in the USA · Silicon Valley. It is the future trading network that that Google venture and bit coin foundation etc. are investing.
										</p>
										<p class="service-description mtop20">
											The traditional settlement method took a huge amount of time and fee, but if you use the ripple network you can realize a transaction close to 0 for a few seconds for remittance time and a cost of 0.
										</p>
										<p class="service-description mtop20">
											Ripple's transaction currency uses XRP and it is possible to exchange from XRP to IOU of various GateWay, and it is expected that economic activity around the world will be carried out quickly.
										</p>
										<p class="service-description mtop20">
											For strong security, overseas banks are beginning to introduce it.
										</p>
<p class="service-description mtop20">
											Reuters: "Ripple has just begun, but it already outperforms bit coins in various ways, and the success of the future has been promised."
										</p>
<p class="service-description mtop20">
											Given the history of bit coins, it is conceivable that value will also rise sharply with the rise in awareness in the world. (At the moment, not everyone can easily purchase it at this moment, it takes time to purchase, even if you purchase it will take time.) As of 2015
										</p>
										<p>
											Currently it is trading on various exchanges.
										</p>
									</div>

								</div>

							</div>

						</div>
						<!--
						<div class="row">

							<div class="col-md-6 col-md-offset-3">

								<div class="service">
									<h4 class="service-name"><img src="img/nofiatcoin_logo.png" ></h4>
									<div class="service-info mleft50">
										<p class="service-description">
											GoldKey（GDK）は、デジタル通貨と現実世界の価値を融合することで、デジタル通貨の世界に新しい価値を生み出します。

										</p>
										<p class="service-description mtop20">
											キーワードは「金」<br>世界中で希少価値が認められている現実世界の金「Gold」と WEB ゴールドと呼ばれている「Bitcoin」で資産価値が保全されています。

										</p>
										<p class="service-description mtop20">
											理念は、「資産の保管庫としての役割」各国々の発行する紙幣の信用が年々減少する中、デジタル通貨に注目が集まっていますが、その中で、デジタル通貨の概念とは逆の、現物資産を所持することでリスクを軽減し、完全なる資産の保管を実現します。

										</p>
										<p class="service-description mtop20">
											そして、Ripple ネットワーク内で保管・流通されているので、セキュリティーと流動性が確保されています。
										</p>
									
										
									</div>

								</div>

							</div>

						</div>
						-->
						<div class="row">

							<div class="col-md-6">

								<div class="service">
									<h4 class="service-name"><img src="img/ethereum_logo.png" ></h4>
									<div class="service-info mleft50">
										<p class="service-description">
											Ethereum (Enterprise) is a platform for distributed applications and is being developed as an open source project since December 2013.
										</p>
										<p class="service-description mtop20">
											Ethereum, based on a technology called "block chain", provides a foundation for realizing various services on P2P systems without any special administrator.
										</p>
										<p class="service-description mtop20">
											That is, Ethereum provides the foundation for realizing a similar service without the need for the presence of centrally managed institutions (private enterprises) such as Facebook, Twitter, Kickstarter and ICANN.
										</p>
										<p class="service-description mtop20">
											And because it is stored and distributed within the Ethereum network, security and liquidity are secured.
										</p>
									
										
									</div>

								</div>

							</div>

							<div class="col-md-6">

								<div class="service">
									<h4 class="service-name"><img src="img/icos_logo.png" ></h4>
									<div class="service-info mleft50">
										<p class="service-description">
											ICO in the virtual currency is "cloud sale".
										</p>
										<p class="service-description mtop20">
											"What is cloud sale?" "Issuing a virtual currency called a unique token and procuring development costs and research expenses by selling it
										</p>
										<p class="service-description mtop20">
											In other words, if you think of it as a stock, it has the same meaning as raising funds by issuing "shares" and purchasing it.
										</p>				
										
									</div>

								</div>

							</div>

						</div>


					<!--	<div class="row">
							<div class="col-md-12 text-center">
								<div class="fontc-degicoin">
									<a href="#purchase"><small>当サイト、Fujicryptoでは煩雑な手続き・送金の一切を代行し、お客様個人のリップル・ビットコインの送金・口座を開設致します。</small></a>
								</div>
								<div>
									<small>（詳しい内容はご利用ガイドを参照して下さい）</small>
								</div>
							</div>
						</div>
					-->
				
				
				
				</div>
				
			</div>

		</section>

		<section id="guide" class="section">

			<div class="section-row-container">

				<div class="section-row">

					<div class="container">

						<div class="row">
							<div class="col-lg-3">
								<h1 class="section-title show-counter">
									USER GUIDE
									<small>Frequently asked questions</small>
								</h1>
							</div>


							<div class="col-lg-8 col-lg-push-1">
								<div class="row">
									<div class="col-md-12">
										<p>
											<span class="fontc-degicoin">AIC ESTONIA</span> provides information on various digital currencies including ICO, and various support.
										</p>
										<p>There is a link on the slider below.</p>
										<p>
										BIT COIN agency store list (It is a list of shops where Bitcoin payment can be made in Global.)</p>
										<p>BIT COIN MACHINE Sales, installation (There is a method of purchasing Bitcoin by ATM by animation.Inquiries concerning the installation of ATM from <a href="<?= $sslUrl ?>contact_input.php">here</a> please
										</p>
									<!--	<p>
											また、パスワードを忘れるなどの事故を防ぐため、当社がウォレットの開設から保管までを一貫して管理することも可能です。ご希望の方は<a href="https://fujicrypto.com/contact_input.html">コチラ</a>よりお問い合わせください。
										</p>
										<p class="mtop50">
											Rippleは海外送金時のレート変動リスクを抑えることができます。（詳しくは下図を参照して下さい）
										</p>-->
									</div>
								</div>

								<!--<div class="row">
									<div class="col-md-12">
										<div class="text-center mtop10">
											<img src="img/guide_image1.png"/>
										</div>
									</div>
								</div>-->

								

						<!--		<div class="row">
									<div class="col-md-12">
										<p class="mtop50">
											<span class="fontc-degicoin">Bitcoin・その他のデジタル通貨をご購入の場合</span>は、送金から口座反映まで2営業日で送金いたします。口座反映は上図でのNormalスキーム同様になりますが他社よりも迅速な取引・確実な送金を行い、送金リスクを考えて1日でも早い口座反映を心掛けております。
										</p>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="text-center mtop10">
											<img src="img/guide_image2.png"/>
										</div>
									</div>
								</div>-->

							　<!--<div class="row">
									<div class="col-md-12">
										<p class="mtop50">
											お客様のウォレットに反映された時点で当サイトの販売業務は終了いたしますが、ご希望であれば決済時の換金も致しておりますのでお気軽に<a href="https://fujicrypto.com/contact_input.html">ご質問</a>ください。
										</p>
										<p class="mtop50">
											最近はとくにこのようなご質問があり、代行依頼が増えています。
										</p>
										<p>
											<span class="fontc-degicoin">・デジタル通貨ってどこで買えるの？</span>
										</p>
										<p>
											<span class="fontc-degicoin">・手続きが面倒くさい、時間がない</span>
										</p>
										<p>
											<span class="fontc-degicoin">・英語が苦手</span>
										</p>
										<p>
											<span class="fontc-degicoin">・本当に送金されるか不安</span><br />
											<small><span class="fontc-degicoin">※</span>FujicryptoはBitcoin・Ripple各系列会社と提携させて頂いているので安心、確実に送金できます。</small>
										</p>
								</div>
							</div>-->
						</div>


					</div>



				</div>

		</section>

		<section class="section" 
			data-background="demo/parallax/2.jpg" data-background-xs="demo/parallax/2-xs.jpg" data-background-md="demo/parallax/2-md.jpg" data-background-lg="demo/parallax/2-lg.jpg">

			<div class="section-row-container">
				<!--
				<div class="section-row th-dark no-padding-bottom bg-transparent">

					<div class="container">

						<div class="row">

							<div class="col-md-12">

								<h3 class="section-subtitle text-center">
									Recent Blog Posts
									<small><span class="highlight">Let's read together</span></small>
								</h3>

							</div>

						</div>

					</div>

				</div>
				-->
				<div class="section-row  half-padding-bottom ptop50">

					<div class="container">

						<div class="row">

							<div class="col-md-12" data-animation-name="fadeInRightBig">

								<div class="owl-carousel items-carousel" data-auto-height="true">

									<div class="carousel-item">
										<a href="agency.php">
										<div class="recent-post post-standard">
											<div class="recent-post-media recent-post-img">
												<div class="media">
													<figure>
														<img src="img/bit_thumb1.png" alt="">
													</figure>
												</div>
											</div>
											<div class="recent-post-body">

												<div class="content">
													<h4 class="post-title">Bit coin Distributor list</h4>
													<p>Introduction of a shop handling settlement with bitcoin.<br /><br/></p>
													<div class="meta">
														<ul>
															<!--<li>19 February 2014</li>
															<li>38 comments</li>
															<li><i class="gi gi-heart text-primary"></i> 22</li>-->
														</ul>
													</div>
												</div>
											</div>
										</div>
										</a>

									</div>

									

									<div class="carousel-item">

										<div class="recent-post post-video">
											<div class="recent-post-media recent-post-video">
												<div class="media">
													<figure>
														<img src="img/bit_thumb2.png" alt="">
													</figure>
													<!--
													<div class="overlay">
														<ul>
															<li class="mfp-zoom"><a href="http://vimeo.com/81151091" class="mfp-iframe"><i class="gi gi-play"></i></a></li>
														</ul>
													</div>
													-->
												</div>
											</div>
											<div class="recent-post-body">
												<!--<a href="blog-single.html" class="read-more-link"><i class="gi gi-link"></i></a>-->
												<div class="content">
													<h4 class="post-title">Bit coin machine sales / installation</h4>
													<p><br />
													<a href="uservideo.php#lamassu">We sell bitcoin machine.<!--bitcoin の販売も行っております。（bitcoinの購入方法はコチラ）--></a>
													<br /></p>
													<div class="meta">
														<ul>
															<!--<li>19 February 2014</li>
															<li>38 comments</li>
															<li><i class="gi gi-heart text-primary"></i> 22</li>-->
														</ul>
													</div>
												</div>
											</div>
										</div>

									</div>

									<div class="carousel-item">

										<div class="recent-post post-standard">
											<div class="recent-post-media recent-post-img">
												<div class="media">
													<figure>
														<img src="img/money_new.jpg" alt="">
													</figure>
													<!--
													<div class="overlay">
														<ul>
															<li class="mfp-zoom"><a href="demo/blog/r1.jpg"><i class="gi gi-resize-full"></i></a></li>
														</ul>
													</div>
													-->
												</div>
											</div>
											<div class="recent-post-body">
												<!--<a href="blog-single.html" class="read-more-link"><i class="gi gi-link"></i></a>-->
												<div class="content">
													<h4 class="post-title">Start exchange</h4>
													<p><a href="exchang.php">We change currency of each country by our company. Please feel free to contact us.</a></p>

													<div class="meta">
														<ul>
															<!--<li>19 February 2014</li>
															<li>38 comments</li>
															<li><i class="gi gi-heart text-primary"></i> 22</li>-->
														</ul>
													</div>

												</div>
											</div>
										</div>

									</div>


								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="section-overlay"></div>

		</section>

		<section id="purchase" class="section">

			<div class="section-row-container">

				<div class="section-row half-padding-bottom">

					<div class="container">

						<div class="row">

							<div class="col-lg-12">

								<h1 class="section-title show-counter">
									ORDER
									<small>Purchase vicarious execution is performed in the e-mail from here</small>
								</h1>

							</div>

						</div>

						<div class="row">

							<div class="col-lg-12">

								<p>
									We are not currently handling.（If you have a question <a href="<?= $sslUrl ?>contact_input.php">here</a>）
								</p>
								
								

							</div>

						</div>

						<!--<div class="row">

							<div class="col-md-12">

								<form method="post" class="form-horizontal contact-form" action="<?= $sslUrl ?>purchase_input.php">
						-->
<!--kiyakubox---------------------------------------------------------------------------------------------------->
<!--
<div class="kiyakubox">
<div class="mtop10 pbottom10 pleft20 pright20 font-line15">
						<p class="mtop10">
フジクリプト株式会社（以下「弊社」といいます）は、その運営するサイト「fujicrypto.com」（以下「当サービス」といいます）の利用規約（以下「本規約」といいます）を以下の通り定めます。当サービスをご利用される方は、以下の内容にご承諾いただいたことになります。
						</p>
						<h5 class="mtop10">■第1章 総則</h5>
						<h6 class="mtop10">第1条 本規約の範囲及び変更</h6>
						<p class="mtop10">本規約は、当サービスの利用に関して、弊社および当サービスの利用者（以下「利用者」といいます）に適用されるものとします。</p>
						<p class="mtop10">弊社は、利用者に事前の承諾を得ることなく、当サービス上で告知あるいは弊社が適当と判断する方法で利用者に通知することにより本規約を変更できるものとします。</p>
						<h6 class="mtop10">第2条 当サービスの利用</h6>
						<p class="mtop10">利用者は、本規約および弊社が別途定めるルール等に従い、当サービスを利用するものとします。弊社は、利用者の事前の承諾を得ることなく、当サービスの内容を変更することができるものとします。</p>
						<h5 class="mtop10">2. 個人情報の利用目的</h5>
						<p class="mtop10">お客さまからお預かりした個人情報は、当社からのご連絡や業務のご案内やご質問に対する回答として、電子メールや資料のご送付に利用いたします。</p>
						
						<h5 class="mtop10">■第2章 サービスの利用</h5>
						<h6 class="mtop10">第3条 利用者情報</h6>
						<p class="mtop10">当サービスの利用に関連して弊社が知り得た利用者の個人情報について、弊社は、個人情報保護の元、管理し、これを利用します。</p>
						<h6 class="mtop10">第4条 サービスの範囲</h6>
						<p class="mtop10">利用者は、当サービス上で提供される全てのサービスを利用できるものとします。また、弊社は、利用できるサービスの範囲を、利用者に事前の承認を得ることなく変更することができるものとします。</p>
						<h6 class="mtop10">第5条 禁止事項</h6>
						<p class="mtop10">利用者は、以下の行為を行ってはならないものとします。</p>
						
						<ol style="list-style-type: decimal"  class="mtop10 pleft30 pright20">
							<li class="mtop10">ご利用の際に虚偽の内容を入力あるいは届出する行為</li>
							<li class="mtop10">当サービスの運営を妨げ、その他当サービスに支障をきたすおそれのある行為</li>
							<li class="mtop10">他の利用者、第三者や弊社に迷惑や損害を与える行為、またはそのおそれのある行為</li>
							<li class="mtop10">他の利用者、第三者や弊社の著作権等の知的財産、プライバシー・人格権その他の権利を侵害する行為、またはそれらのおそれのある行為</li>
							<li class="mtop10">公序良俗に反する行為その他法令に違反する行為、またはそれらのおそれのある行為</li>
							<li class="mtop10">その他、弊社が不適当と判断する行為</li>
						</ol>
						
						<h6 class="mtop10">第6条 当サービスの利用停止</h6>
						<p class="mtop10">弊社は、利用者が以下の各号のいずれかに該当する場合、利用者に事前通知することなく当サービスの提供を利用停止することができるものとします。</p>
						
						<ol style="list-style-type: decimal"  class="mtop10 pleft30 pright20">
							<li class="mtop10">第5条（禁止事項）の行為を行った場合</li>
							<li class="mtop10">その他本規約に違反した場合</li>
						</ol>
						
						
						<h6 class="mtop10">第7条 著作権</h6>
						<p class="mtop10">利用者は、当サービスを通じて提供されるいかなる情報も、弊社の許諾を得ないで、著作権法で認められる個人の私的複製等著作権の制限規定範囲外での使用をすることはできません。本条の規定に違反して問題が生じた場合、利用者は自己の責任と費用においてかかる問題を解決するとともに、弊社に迷惑または損害を与えないものとします。</p>
						
						
						<h5 class="mtop10">■第3章 商品の購入</h5>
						<h6 class="mtop10">第8条 商品の購入</h6>
						<p class="mtop10">利用者は、当サービスを利用して弊社が商品購入代行をすることにより商品またはサービス（以下「商品等」といいます）を購入することができます。利用者は、商品等の購入を希望する場合、弊社が指定する方法、ルールに従って商品の購入またはサービスの利用を申込むものとします。利用者は、申込み内容を真実かつ正確なデータを入力し、送信するものとします。申込に対して、弊社が承諾する場合、利用者が記入した電子メールアドレス宛に承諾する旨を電子メールで送信した時をもって注文受付とさせていただきます。</p>
						<p class="mtop10">前項の規定に拘わらず、当サービス利用に関して不正行為もしくは不適当な行為があった場合、弊社は、購入代行やサービスの提供を取消もしくは解除、履行停止その他適切な措置を取ることができるものとします。</p>
						<h6 class="mtop10">第9条 決済方法</h6>
						<p class="mtop10">商品等のお支払い金額は、商品購入代金、取扱手数料および消費税の合計となります。
当サービスによって購入された商品等のお支払いに関しては、利用者本人名義の銀行から弊社指定口座へのお振込みによるお支払いによるものとします。</p>
						<h6 class="mtop10">第10条 商品の価格</h6>
						<p class="mtop10">商品購入代金は弊社の代行手数料および商品代行して購入した時価での価格となります。</p>
						<h6 class="mtop10">第11条 注文のキャンセル</h6>
						<p class="mtop10">利用者は、商品の購入代行のご依頼後、利用者が商品購入金額を弊社にご送金頂く前まではご注文のキャンセルができます。</p>
						<p class="mtop10">以下の商品は、注文のキャンセルを一切お受けできません。</p>
						
						<ol style="list-style-type: decimal"  class="mtop10 pleft30 pright20">
							<li class="mtop10">商品購入金額を弊社の指定する銀行に送金した後</li>
							<li class="mtop10">購入代行済みの商品</li>
							<li class="mtop10">利用者が商品等の購入依頼の際に、キャンセル不可であることが商品ページに明記している商品</li>
						</ol>
						
						<h6 class="mtop10">第12条 商品等に関する免責</h6>
						<p class="mtop10">当サービスにおいて購入代行や提供するサービスに関する保証は、その価値、品質、性能、他の商品等との適合性その他のいかなる保証も行いません。弊社は、いかなるトラブルに関しても利用者からお知らせ頂いた、連絡先へ連絡すること、および商品購入代行の際指示した送付先に商品等を提供することにより免責されるものとします。</p>
						<p class="mtop10">弊社が利用者から承った商品代行依頼について、弊社または提供元が入手不可能となった場合、弊社より当該ご注文を解除できるものとします。その際、弊社は利用者の銀行口座に購入金を全額返金するものとします。</p>
						<p class="mtop10">弊社は、当サービス上での商品説明あるいは表記については、できる限り正確性を期しておりますが、正確性、完全性、最新性等に一切誤りがないことを保証するものではありません。現状と異なる表記があった場合は現状を優先します。</p>
						
						<h5 class="mtop10">■第4章 当サービスの運用</h5>
						<h6 class="mtop10">第13条 情報の管理</h6>
						<p class="mtop10">弊社は、利用者が当サービスに対し弊社が利用することを承諾した上で発信したコメントその他の情報（個人を特定する情報は第3条にしたがいます）について、弊社が利用可能と判断した場合は、利用者に断りなく当サービスあるいは弊社の提携会社において利用できるものとし、次の各号の一つにでも該当する場合には、利用者に断りなくこれを削除することができるものとします。これまでに当サービスで弊社宛に発信していただいたコメントその他の情報についても、発信された利用者から特段の申し入れが無い限り、弊社所定の方法で当サービスあるいは提携会社において利用できるものとします。</p>
						
						<ol style="list-style-type: decimal"  class="mtop10 pleft30 pright20">
							<li class="mtop10">当該情報が弊社もしくは第三者の著作権その他の権利を明らかに侵害し、または弊社もしくは第三者の名誉もしくは信用を明らかに毀損していると認められた場合</li>
							<li class="mtop10">当該情報が第三者の著作権その他の権利を侵害し、または第三者の名誉もしくは信用を毀損しているとの警告を、弊社が当該第三者から受取った場合</li>
							<li class="mtop10">官公庁、裁判所等の公的機関から、法律に基づき削除するよう命令を受けた場合</li>
						</ol>
						
						<h6 class="mtop10">第14条 当サービスの保守</h6>
						<p class="mtop10">弊社は、当サービスの稼動状態を良好に保つために、以下各号の場合利用者に事前に通知を行うことなく当サービスの提供の全部あるいは一部を中止することができるものとします。なお、弊社はかかるサービスの中止により、利用者に発生した損害、不利益に関して一切責任を負わないものとします。</p>
						
						<ol style="list-style-type: decimal"  class="mtop10 pleft30 pright20">
							<li class="mtop10">システムの定期保守および緊急保守の場合</li>
							<li class="mtop10">火災、停電、第三者による妨害行為等により、システムの運用が困難になった場合</li>
							<li class="mtop10">その他、止むを得ずシステムの停止が必要と弊社が判断した場合</li>
						</ol>
						
						<h6 class="mtop10">第15条 その他免責事項</h6>
						<p class="mtop10">弊社は、利用者が当サービスをご利用になれなかったことにより発生した一切の損害について、いかなる責任も負わないものとします。弊社は、利用者の受けた不利益等が利用者の登録内容に従い事務を処理したことによる場合には免責されるものとします。</p>
						<p class="mtop10">利用者が、当サービスをご利用になることにより、他の利用者または第三者に対して損害等を与えた場合には、当該利用者は自己の責任と費用において解決し、弊社には一切迷惑を与えないものとします。</p>
						
						<h6 class="mtop10">第16条 その他</h6>
						<p class="mtop10">弊社と利用者との連絡方法は、原則として電子メールによるものとし、利用者はこれを確認するものとします。</p>
						<p class="mtop10">当サービスのご利用に関して、本規約または弊社の指導により解決できない問題が生じた場合には、弊社と利用者との間で双方誠意をもって話し合い、これを解決するものとします。</p>
						<p class="mtop10">当サービスの利用に関して訴訟の必要が発生した場合には、東京地方裁判所または東京簡易裁判所を第一審の専属管轄裁判所といたします。</p>
						
			</div>
			-->
<!-- /.section --></div>
<!--kiyakubox---------------------------------------------------------------------------------------------------->

						<!--
									<div class="form-group">
										<div class="col-md-12">
											<input type="submit" value="利用規約に同意して注文する" class="btn btn-sm btn-block btn-primary">
										</div>
									</div>
								</form>

							</div>


						</div>
						-->

					</div>

				</div>

			</div>

		</section>

<div id="sample" class="hide">
    <p>sample</p>
</div>

		

		

	</div>

	<footer class="site-footer">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="footer-logo">
						<a href="#"><img src="img/degicoin_logo.png" alt=""></a>
					</div>

					<div class="text-center">
					<a href="#"><small>Online selling agreement</small></a>　　
					<a href="https://aicestonia.ee/privacy-policy.php"><small>Privacy policy</small></a>　　
					<a href="https://aicestonia.ee/sctl.php"><small>Specific Transactions</small></a>　　
					</div>

					<div class="text-center">
					<small>Copyright aicestonia.ee</small>
					</div>

					<div class="social-list">
						<ul>
							<!-- List of social icons here -->
							<li><a href="#"><i class="{social icon class here}"></i></a></li>
						</ul>
					</div>

				</div>

			</div>

		</div>

	</footer>

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="assets/js/hydrogen.setup.min.js"></script>
<script type="text/javascript" src="assets/js/hydrogen.scripts.min.js"></script>
<script type="text/javascript" src="assets/js/hydrogen.plugins.js"></script>

<script type="text/javascript" src="assets/plugins/royalslider/jquery.royalslider.min.js"></script>
<script type="text/javascript" src="assets/plugins/mfp/jquery.mfp-0.9.9.js"></script>
<script type="text/javascript" src="assets/plugins/mediaelement/mediaelement-and-player.min.js"></script>
<script type="text/javascript" src="assets/plugins/gmap/gmap3.min.js"></script>
<script type="text/javascript" src="assets/plugins/owlcarousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/plugins/isotope/jquery.isotope.min.js"></script>
<script type="text/javascript" src="assets/plugins/form/jquery.form.min.js"></script>
<script type="text/javascript" src="assets/plugins/form/jquery.validate.min.js"></script>

<script type="text/javascript" src="assets/js/jquery.cookieBar.js"></script>


<script>
$(function() {
	$.cookieBar({
		style: 'bottom'
		});
});
</script>
</body>

</html>