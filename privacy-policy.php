<?php
//ini_set( 'display_errors', 1 );

session_start();
# Set configlation
include_once "config.php";
// $url,$sslUrl,$ref
# Set navigation
//include_once "navigation.php";
// top,normal
# Set analytics
//include_once "analyticstracking.php";
// googleAnalytics,facebookAnalytics,googleRemarketing,ymdRemarketing,facebook
# Set rate needs to top page only
include_once "global_rate.php";
//USD,JPY,XRP
?>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="">
<meta name="author" content="">

<title>AIC ESTONIA</title>

<!-- Fonts -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
<link rel="stylesheet" href="assets/fonts/novecento/stylesheet.css">
<link rel="stylesheet" href="assets/icons/glyphicons/style.min.css">
<link rel="stylesheet" href="assets/icons/font-awesome/font-awesome.min.css">

<!-- Styles -->
<!--CHANGCE COLOR sreyneang test color-->
<link rel="stylesheet" href="assets/css/style2.css">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/style.min.css">
<link rel="stylesheet" href="assets/css/theme-light.css">
<!--<link rel="stylesheet" href="assets/css/theme-dark.css">-->
<link rel="stylesheet" href="css/mystyle.css">
<link rel="stylesheet" href="css/degicoin.css">

<!-- GDPR -->
<link rel="stylesheet" href="assets/css/jquery.cookieBar.css">

<!-- Plugins -->
<link rel="stylesheet" href="assets/plugins/royalslider/royalslider.min.css">
<link rel="stylesheet" href="assets/plugins/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" href="assets/plugins/mfp/jquery.mfp.css">

<!-- Additional styles -->
<style>
#culture {
	background-image: url(demo/img/culture-bg.jpg);
}
</style>

</head>

<body>

<div class="site-loader"></div>
<header class="site-header th-dark">

	<div class="header-inner">

		<div class="container">

			<div class="row">

				<div class="header-table col-md-12">

					<div class="brand">
						<a href="#">
							<img src="img/degicoin_title.png" alt="AIC">
						</a>
					</div>

					<nav class="main-nav">
						<a href="#" class="nav-toggle"></a>
						<ul class="nav">
							<!-- Menu items here -->
							<li><a href="#">Home</a></li>
							<!--<li><a href="http://fujicrypto.com/session.html">Session</a></li>-->
							<!--<li><a href="#about">About</a></li>-->
							<li>
								<a href="#about">
									About Menu
									<span class="sub-toggle"></span>
								</a>
								<ul>
									<li><a href="#about">About</a></li>
									<li><a href="<?= $url ?>corp.php">Corporation</a></li>
									<li><a href="<?= $url ?>seminar.php">Seminar</a></li>
									<li><a href="<?= $url ?>news.php">News</a></li>
									<li><a href="<?= $url ?>attention.php">Attention</a></li>
								</ul>
							</li>
							
							<li>
								<a href="#currency">
									Currency Menu
									<span class="sub-toggle"></span>
								</a>
								<ul>
									<li><a href="#currency">Currency</a></li>
									<!--
									<li><a href="http://gdk.fujicrypto.com/">goldkey</a></li>
									-->
									<!--
									<li><a href="bitcoin.html">bitcoin</a></li>
									<li><a href="ripple.html">ripple</a></li>
									-->
								</ul>
							</li>
							<li><a href="<?= $url ?>exchang.php">Exchange</a></li>
							<!--<li><a href="#currency">Currency</a></li>-->
							<!--<li><a href="#guide">Guide</a></li>-->
							<li>
								<a href="#guide">
									Guide Menu
									<span class="sub-toggle"></span>
								</a>
								<ul>
									<li><a href="#guide">Guide</a></li>
									<li><a href="<?= $url ?>uservideo.php">Video</a></li>
								</ul>
							</li>
							<li><a href="#purchase">Order
									<span class="sub-toggle"></span>
								</a>
								<!--
								<ul>
									<li><a href="<?= $sslUrl ?>sctl.php">Specified Commercial Transactions</a></li>
								</ul>
								-->
							</li>
							<li><a href="<?= $sslUrl ?>contact_input.php">Contact</a></li>
							<!--<li><a href="https://fujicrypto.com/pamphlet_input.html">Pamphlet</a></li>-->
							<!--
							<li>
								<a href="#">
									Multilevel Menu
									<span class="sub-toggle"></span>
								</a>
								<ul>
									<li><a href="#">Multilevel Menu</a></li>
									<li><a href="#">Multilevel Menu</a></li>
								</ul>
							</li>
							-->
						</ul>
					</nav>

				</div>

			</div>

		</div>

	</div>

</header>
<section id="contact" class="section">

			<div class="section-row-container">

				<div class="section-row half-padding-bottom">












					<div class="container">

						<div class="row">

							<div class="col-lg-12">
<!--
								<h1 class="section-title show-counter">
									PURCHASE
									<small>Purchase vicarious execution is performed in the e-mail from here</small>
								</h1>
-->
				<h2 class="section-separator-title">
					<span data-shadow-text=" PRIVACY POLICY">PRIVACY POLICY</span>
				</h2>

							</div>

						</div>

						<div class="row">

							<div class="col-md-12">

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>Welcome
to the Privacy Policy section of AIC ESTONIA &#8211; div (&quot;Policies&quot;). For
improved interpretation of these Policies, all definitions, terms and
conditions of the Terms of Use of AIC ESTONIA - div apply to the terms. You
must comply to visit this site regularly, to notice any changes and amendments AIC ESTONIA
does.</span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>INTRODUCTION.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>Your
privacy is important to AIC ESTONIA and its affiliates. The purpose of this
Privacy and Cookie Policy (&quot;Policy&quot; or &quot;Policies&quot;) is to
keep Users informed on how AIC ESTONIA manages all personal data which we gather
from any User of the AIC ESTONIA Websites and Services, all in strict accordance
with the Personal Information Protection Act (gthe Acth). By interacting with
the AIC ESTONIA Websites, using and accessing our services, registering and
signing up for a AIC ESTONIA account, and sending us documents, images and other
material to verify a AIC ESTONIA Account, the User agrees to these Policies and
consents to AIC ESTONIA the managing of all gathered information. These Policies
do not supersede or replace the Terms of Service Users have consented to,
unless determined otherwise in the Whole Agreement. All definitions determined
in the Terms of Service of AIC ESTONIA remain applicable and shall be used
throughout these Policies. We encourage Users to visit this Section of the
Website often, to notice any possible change which we may post, and to read
them thoroughly in every visit. Also this Privacy Policy intends to be
compliant with the determinations of the gPersonal Information Protection
Committeeh, to be created in 2016.</span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>PERSONAL INFORMATION AIC ESTONIA COLLECTS.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>AIC ESTONIA
collects all the Personal Information that Users provide directly to AIC ESTONIA
whenever they register or signup for a AIC ESTONIA Account, they provide scanned
documentation for verification purposes, and when Users perform transactions on
the Platform, or use other AIC ESTONIA Services. This personal information may
include:</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>I.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>GENERAL CONTACT INFORMATION. This general
information about users include, but are not limited to, complete names, home
addresses or addresses of Entities or Companies when applicable, and email
addresses.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>II.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>ACCOUNT INFORMATION. These are usernames and
passwords, and numbers, addresses or alphanumeric of devices used for Two
Factor Authentication purposes.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>III.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>FINANCIAL INFORMATION. These are mainly Bank
account numbers, Bank statements, Digital Commodities account or wallet codes,
and other trading information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>IV.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>IDENTITY VERIFICATION INFORMATION. These
consist of images of any government issued ID, passports, national ID card,
driving license and in some cases voting identification and social security
numbers, when applicable.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>V.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>RESIDENCE VERIFICATION INFORMATION.&nbsp;
These can be Utility bills or others which advocate for the User's residency
and address. </span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>AIC ESTONIA
may also collect certain computer, device, and browsing information when Users
access the AIC ESTONIA Websites or the platform. This information is merely
statistical of Users behavior and tendencies or preferences whilst browsing or
engaging in trading activities on AIC ESTONIA's websites and/or platform, and
the gathered information is meant to improve all AIC ESTONIA's services to
better suit Users. </span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>HOW WE SHARE PERSONAL INFORMATION WITH
OTHER PARTIES.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>User
agrees and authorizes AIC ESTONIA, to share any and all information collected
from User to or with the following:</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>I.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>Service Providers under an agreement or
engaged by AIC ESTONIA, who help with key parts of the operations of AIC ESTONIA,
such as fraud prevention, documentation collection, documentation verification,
people/user comparison, address verification, and also marketing and technology
services. AIC ESTONIA warrants to all Users that the only information shared
with these third party Service Providers will be exclusive of the specific
services that they provide, and that AIC ESTONIA shall intend to keep such
sharing to the minimum as for the Service Providers to be able to fulfill their
engagements and agreements; </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>II.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>Financial institutions with which AIC ESTONIA
has partnered or is in an agreement with, related to the provision of the AIC ESTONIA
Services.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>III.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>Companies which are planning to acquire AIC ESTONIA,
or Companies which AIC ESTONIA might merge with, or other legal figures which
would oblige AIC ESTONIA to Share User's information with them. If any such
event happens, any Company will be enforced to be abiding and respectful of
these Privacy and Cookie Policies.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>IV.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>Whenever the User consents or directions AIC ESTONIA
to share this information for whatever purpose which is not illegal or against
the Terms and Conditions of Use, these Policies, or the KYC/AML policies. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>V.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>Any governmental institutions with
jurisdiction, including but not limited to law enforcement, government
officials, or other&nbsp; such third parties which may have jurisdiction of AIC ESTONIA
or the Users, whenever:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>VI.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>AIC ESTONIA is required to share Users or a
specific User's information, either by a subpoenas, court orders or other
similar legal procedures; or if AIC ESTONIA suspects in good faith that the
disclosing of such information is critical to prevent physical harm or
financial loss by any party involved in this agreement, or when AIC ESTONIA
suspects that the User might be engaging in illegal activities, whatever their
nature or jurisdictional proceeding, or when AIC ESTONIA suspects or has good
reason to believe that the User or group of Users is somehow breaching the
Terms and Conditions of Use of AIC ESTONIA, these Policies, the KYC/AML
policies, or other agreements with AIC ESTONIA. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;font-family:"Raleway",sans-serif'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>PROTECCION OF YOUR INFORMATION.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>We
utilize encryption/security software to safeguard and shield the
confidentiality of your privacy and personal information; in addition to the
aforementioned, we may manage and maintain electronic, physical, technical and
procedural protection and practices to ensure your information is as save as
possible. Even so, you hereby agree and acknowledge that the internet is not a
safe place or platform, far away, no website is absolutely secure, despite of
any efforts to prevent this and what web designers and programmers assert about
the subject; this implies that as now place or site, or software currently
running or hosted on the internet secure, you understand that, despite of our
efforts, your information may be accessed, shared or used in any way, due to a
breach or forceful entry or access to the Sites, or to anyone altering our
systems, or depositing any sort of malware or malicious software on our
servers. What is stated before in the preceding sentence does not count towards
breaches done to your account, and unauthorized access to your own equipment,
due to negligence, carelessness or other motives solely of your
responsibility.&nbsp;&nbsp; </span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>AIC ESTONIA
also protects your information by having regular revisions to its procedures
and policies, to improve security and lowering the chances of breaches.
Employees are increasingly capacitated on the matter of the handling of private
information.</span></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
font-family:"Raleway",sans-serif'>&nbsp;</span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>THIRD PARTY LINKS.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>At
times, due to reasons difficult to foresee, links or hyperlinks may appear on
the Websites which are under AIC ESTONIA's control, and which may be made
available by either by AIC ESTONIA its Users, or other third parties. Please
remember that these links which lead away from the AIC ESTONIA controlled
websites are not under AIC ESTONIA's control, and may have their own independent
privacy and cookie policies, as well as their own and independent Terms of Use,
and/or other applicable agreements and policies. AIC ESTONIA has no responsibility
or liability of the content, the services, or any other information made
available on these third party websites, and you will be solely responsible for
your activities and your information whenever opt to enter any of such third
party websites.</span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>MARKETING.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>AIC ESTONIA
will never rent, sell, or disclose commercially any of the User's provided
information. From time to time AIC ESTONIA may combine information with other
companies as to improve its content and advertising, and to personalize it more
for its Users.</span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>USE OF COOKIES.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>The
AIC ESTONIA websites use cookies. AIC ESTONIA will use these cookies mainly to
better understand users and their preferences while navigating on the websites,
or to remember users visits and their most frequented pages visited. Cookies
are pieces of information which the User's choice of browser or other software
install on the Userfs hard drive. They are mainly used to correctly recognize a
computer which has passed or logged on a particular device or computer. Cookies
tend to make User's experience better whilst on the AIC ESTONIA Websites.
However, you may choose, through you browser's control panel, preference
section, or other similar options, to not to accept cookies from a particular
website, or of all websites. We warn any Users that, although they are free to
opt not to accept cookie of the AIC ESTONIA Websites, their user experience may
get more difficult or slow, and some parts of the website may not be available
for them, or function with errors. </span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>ACCESS OF USERS TO PERSONAL INFORMATION.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>All
Users have right to access their own shared personal information, to either
update, correct or otherwise change their information and data. If a User
decides to do so, the User must contact AIC ESTONIA to support@div.co and AIC ESTONIA
shall respond to the requirement in a reasonable period of time, never
surpassing 20 days, and only if pursuant to the Terms and Conditions of Use of AIC ESTONIA,
or to other policies. To prevent theft of information, AIC ESTONIA may ask any
User to provide proof of identification, as to prevent identity theft and other
illegal activities.</span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span lang=EN-US style='font-size:12.0pt;line-height:
115%;font-family:"Raleway",sans-serif'>CHANGES TO THESE POLICIES.</span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif'>AIC ESTONIA
reserves all rights to, revise, amend, modify, change, improve, supplant or do
any alterations AIC ESTONIA deems necessary, in any manner and at any time, as
to be law compliant or to improve AIC ESTONIAfs policies and protect User's
interests. All Users are advised to frequently visit this section of the
Websites, as to notice any changes; AIC ESTONIA will always put the last date
when any form of changes where applied to these policies. Also AIC ESTONIA may,
under no obligation whatsoever, choose to notify Users of such changes, to
their provided email addresses. User's continuing use of the AIC ESTONIA
Websites, Services and platform shall be construed as their acceptance of all
changes or amendments made to these Policies. </span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

</div>

<!--
								<p class="lead">PLEASE READ THESE TERMS OF SERVICE CAREFULLY AND RESPONSIBLY. BY YOUR CONTINUED USE OF THIS WEBSITE AND ITS SERVICES AND YOUR OPENING AND USING THE SERVICES PROVIDED ON THIS WEBSITE, YOU AGREE TO BE BOUND BY THE TERMS DESCRIBED HEREIN AND ALL TERMS INCLUDED AS REFERENCE, INCLUDING WITHOUT LIMITATION THE KYC/AML AND PRIVACY AND COOKIE POLICIES. IF YOU DO NOT AGREE TO THESE TERMS, PLEASE IMMEDIATELY DISCONTINUE THE USE OF THIS WEBSITE, YOU ACCOUNTS AND THE SERVICES PROVIDED BY THE AIC ESTONIA PLATFORM.</p>

								
								<h4 class="ptop40">1.INTRODUCTION</h4>
								<p>AIC ESTONIA INC. is a registered Company incorporated in Singapore under Registration No. 201425404W. AIC ESTONIA INC., and all its subsidiaries and affiliate companies (altogether referred to as “AIC ESTONIA”, “we”, “our” or “us”) is the owner and administrator of the website located at http://www.div.co (the “Site”, “Website” or “Websites”). AIC ESTONIA is a company legally constituted in strict appliance of all applicable corporate law in Singapore, and all of its applicable statutes, regulations and Acts. You, the User, (“User”, “Trader”, “Visitor”, “you”, “your”) expressly agree and represent that you have read and understand that these Terms of Service and any terms expressly incorporated herein (altogether referred to as "Terms", “Agreement” or “Contract”), as apply to your access to and use of the Websites, and all of your trading and direct sale services which AIC ESTONIA makes available through its platform as described in these Terms (collectively, our "Services").</p>

								<h4 class="ptop40">2.IMPORTANT DEFINITIONS</h4>
								<p>AIC ESTONIA INC. is a registered Company incorporated in Singapore under Registration No. 201425404W. AIC ESTONIA INC., and all its subsidiaries and affiliate companies (altogether referred to as “AIC ESTONIA”, “we”, “our” or “us”) is the owner and administrator of the website located at http://www.div.co (the “Site”, “Website” or “Websites”). AIC ESTONIA is a company legally constituted in strict appliance of all applicable corporate law in Singapore, and all of its applicable statutes, regulations and Acts. You, the User, (“User”, “Trader”, “Visitor”, “you”, “your”) expressly agree and represent that you have read and understand that these Terms of Service and any terms expressly incorporated herein (altogether referred to as "Terms", “Agreement” or “Contract”), as apply to your access to and use of the Websites, and all of your trading and direct sale services which AIC ESTONIA makes available through its platform as described in these Terms (collectively, our "Services").</p>

								<ol>
									<li>1."Financial Account" means any financial account of which you are the beneficial owner that is maintained by a third party outside of the Services, including, but not limited to third-party payment service accounts or accounts maintained by third party financial institutions or third part which provide financial related services to AIC ESTONIA.</li>
									<li>"Funds" means Virtual Commodities and/or Legal Tender.</li>
									<li>"Legal Tender" means any national and/or fiat currency, such as Japanese Yens, which may be used in connection with a purchase or sale of Virtual Commodities through the use of the Services, and does not include any Virtual Commodities.</li>
									<li>"Funds" means Virtual Commodities and/or Legal Tender.</li>
								</ol>


								
								<ul>
									<li>弊社はデジタル通貨の専門会社です。未公開株や社債などの金融商品は一切扱っておりません。</li>
									<li>弊社からデジタル通貨を購入する場合は、必ずフジクリプト名義の銀行口座にお振込み下さい。弊社が他の名義の銀行口座を指定することはありません。</li>
									<li>また、勧誘に少しでも不審と思われる場合は、弊社、又は最寄りの警察にご相談ください。</li>
								</ul>
							-->
							</div>

						</div>

					</div>

				</div>

			</div>


		</section>

		<section id="contact" class="section">
			<div class="section-row-container">
				<div class="section-row half-padding-bottom">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
				<h2 class="section-separator-title">
					<span data-shadow-text=" AML &amp; KYC POLICIES">AML &amp; KYC POLICIES</span>
				</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">


<p class=MsoListParagraph style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><b><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>A.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><u><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Introduction.-</span></u></b></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>AIC ESTONIA
INC. (hereinafter referred to simply as gAIC ESTONIAh) is a company legally
incorporated in Singapore, and is voluntarily committed to comply with any and
all applicable Anti-Money Laundering and Countering Financing of Terrorism
Legislation Acts and Regulations (AML/CTF Laws) even when such compliance is
deemed unnecessary in some cases. To achieve this goal, Japanese law requires
local companies to correctly obtain, verify, and record information that
identifies each person opening an account, either it be a natural person or a
corporate entity. AIC ESTONIA has developed internal policies addressing AML and
KYC compliance by the company (hereinafter simply referred to as gPoliciesh) which
are based on risk assessment, so the objectives of any local and international
applicable laws regarding AML/CTF &amp; KYC are met. </span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>The
targets of these Policies may include, but are not limited to:</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Attempt and cooperate with local and international initiatives
in the Detection and prevention of money laundering and financing of terrorist
organizations and activities;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Maintain a clear image and being transparent and cooperative
with any sort of local or international institutions by following, where
appropriate, recommendations made by institutions which fight money laundering
and financing of terrorism, including, but not limited to, the Financial Action
Task Force on Money Laundering;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Explaining and warning all users of AIC ESTONIA of the measures
which are being taken by AIC ESTONIA to remain being an honest and law complying
company; and</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Remain compliant of any current and/or future applicable local
and international laws towards KYC, AML/CFT and CDD.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Monitor and Report any suspicious and unusual activities and/or
transaction made by their users which may be related to any illegal activities
which occur on the website to authorities, depending on jurisdiction and place
of occurrence. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Making an effort to comply to all legal requirements regarding
AML/CFT/KYC regulations on an local and international level.</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><b><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>B.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><u><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Terms
of agreement of these policies</span></u></b></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>By
applying for an account with AIC ESTONIA, users represent to agree to the
following terms and conditions:</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>The user will warrant to AIC ESTONIA and to their respective
governments that whilst accessing and using the services provided on the AIC ESTONIA
platform and website they will comply with all applicable laws regarding
AML/CTF, and that the user will not engage in any fraudulent, illegal and
criminal activities, of any local laws, or the laws of their jurisdiction:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>The user agrees to provide to AIC ESTONIA any requested
documentation requested by AIC ESTONIA at any point, to be able to comply with
local and international legislation regarding to AML and CTF.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>The user represents that all data, information and documentation
provided to AIC ESTONIA when applying for an account is true and verifiable, and
that the user will hold AIC ESTONIA immune of any liabilities or legal claims of
any party whatsoever, either private or public, in the event that any data,
information and documentation is found to be untrue on a later verification,
done either by AIC ESTONIA, Government authorities or enforcement agents or by
other third parties. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>The user will cooperate with AIC ESTONIA and any authorities with
any investigation or requests in relation of their use of the services on AIC ESTONIA
by providing documentation or other form of legal proof regarding their
identities and their transactions on the AIC ESTONIA Platform; furthermore they
warrant that they will not withhold any important information, data or documents
in relation with their use and access of the website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>The user represents that he/she understands that due to
applicable AML/CTF laws, AIC ESTONIA will be entitled and furthermore obligated
to suspend or close any user accounts related to a suspicious, infringing or
unusual activities. In the event these activities come from a Country where AIC ESTONIA
does not operate in, AIC ESTONIA may prevent users from opening an account
altogether. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>The user represents that he/she, that In compliance of these
Policies, AIC ESTONIA will suspend or close accounts of a user who fails to
provide the necessary documentation to back up their activities on the Website
and/or Identity; this enables AIC ESTONIA to proceed at any point whatsoever, either
during registration and at AIC ESTONIAfs reasonable interpretation and suspicion,
in good faith, of the userfs provided registration data. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>The user represents that all the money or digital commodities
used to fund or deposit in any AIC ESTONIA accounts is not, and will not be
related in any manner to activities related to Money Laundering, Terrorist
activities or financing, or any other sort of illegal activities or behaviors
which are deemed contrary to local and international law. AIC ESTONIA strongly
cautions its users to verify sources of their money and digital commodities, as
to not to enter in legal infringements. </span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>The user warrants to AIC ESTONIA that the money and digital
commodities will not be used to engage in money laundering related activities
or to the financing of terrorist organizations, or to fund any other sort of
illegal activities.</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><b><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>C.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><u><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>User
Information Requirements.-</span></u></b></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>In
strict accordance with these policies, AIC ESTONIA will conduct an initial and
ongoing verification and due diligence of its platform users, always responding
to the risk levels which each particular user may represent. In this purpose
and to satisfy KYC requirements and policies, AIC ESTONIA will ask for userfs
identification information to every user who applies for an account on the
website, and will store and secure all data gathered; this will also apply to
the results and outcome of the verification process. Whenever AIC ESTONIA asks
for identification information, AIC ESTONIA will notice itfs users of such
request through the provided email address. AIC ESTONIA, compliant o any applicable
local and international regulation, may also compare the information with
governmental lists, to make sure they have not been engaged in any way in any
illegal activities in relation to Digital Commodities, finance and tax laws, or
that users may have been related to suspected terrorist activities. If there
were reasonable belief on AIC ESTONIAfs part of the identity of the user, they
will notify and request clarification or more documentation of the user. If the
outcome represents high risk for AIC ESTONIA, they may deny an account opening,
or suspend or close any existing accounts of that user. Also, AIC ESTONIA may
determine whether the prospect or existing user is or becomes a domestic or
foreign Politically Exposed Person (PEP), and if additional paperwork would
need to be filed. AIC ESTONIA may refuse any transactions at any time made by
users where verification is pending, or whenever they find that a particular
transaction will need more information to make sure it is compliant with
applicable law. </span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Specific data required of Natural Person Users</span></u></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>The
requested information and data of the user which is a natural person includes,
but is not limited to:</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;color:#0F0F0F'>&middot;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Full
name.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Date and place of birth.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Marital Status.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Nationality.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Address.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Occupation and information of employer or self-employment circumstances.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;color:#0F0F0F'>&middot;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Expected
origin of the funds to be used on the AIC ESTONIA Platform.</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Specific data required additionally of Corporate Users</span></u></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>In case
the User is a Company, in addition of the previous documentation of the person
acting in legal representation and/or as an authorized registrar of the
Company, the following information and data will include, but is not limited
to:</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;color:#0F0F0F'>&middot;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Company
identifier or registration number.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Registered corporate name.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Registered Address and Post N&ordm; of the Company.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;color:#0F0F0F'>&middot;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Place
of incorporation.</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Documentation required for verification of natural person user
data</span></u></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>So that
AIC ESTONIA is able to verify the provided information given and provided by the
user, AIC ESTONIA will require A SCANNED READABLE DOCUMENT. The documents may
vary depending on a risk assessment. These may include, but are not limited to:</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;color:#0F0F0F'>&middot;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Current
and Valid Passport with a Clear Color photograph.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Current and Valid Government issued National Identity Card with
a Clear Color photograph.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Current and Valid Driverfs License with a Clear Color photograph.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Election ID card with a Clear Color photograph.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Credit Card Statements with coinciding name and address.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Salary Slip with coinciding name and address.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Electricity Bill with coinciding name and address.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Income / Wealth tax Assessment Order with coinciding name and
address.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>User Local certificate of address issued by a governmental
authority with coinciding name and address.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;background:white'><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Documentation required for verification of corporate user data</span></u></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><u><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'><span
 style='text-decoration:none'>&nbsp;</span></span></u></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>So that
AIC ESTONIA is able to verify the provided information given and provided by the
user, AIC ESTONIA will require A SCANNED READABLE DOCUMENT. The documents may
vary depending on a risk assessment. These may include, but are not limited to:</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><u><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'><span
 style='text-decoration:none'>&nbsp;</span></span></u></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;color:#0F0F0F'>&middot;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Certificate
of incorporation, or any national equivalent.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Memorandum and Articles of incorporation or association or any
national equivalent.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Statutory Statement of the Company or any national equivalent.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>A certificate of good standing issued by a governmental
organization.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Copies of powers of attorney or other documents that proof that
the representation made by the user is valid and legal.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0mm;margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;
background:white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;
color:#0F0F0F'>&middot;<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>Proof of identity of directors in case they were assigned to use
and access the services on AIC ESTONIAfs platform in legal representation of the
Company.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><span lang=EN-US style='font-size:12.0pt;font-family:Symbol;color:#0F0F0F'>&middot;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Proof
of identity of the beneficial owner in case they were assigned to use and
access the services on AIC ESTONIAfs platform in legal representation of the
Company.</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;font-family:"Raleway",sans-serif;
color:#0F0F0F'>AIC ESTONIA will require of its users any and most of the
documents listed above, and determine a risk assessment on an each-case base.
Most of the documents listed above will be required when the user attempts to
open an account on AIC ESTONIAfs Website. Others may be required depending on
the risk level resulting from the verification of the documents. Accounts may
be limited or only accessible for review until the account has been approved to
use the platform. </span></p>

<p class=MsoListParagraph style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><b><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>D.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><u><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Use
of the submitted documents and information made by the users. </span></u></b></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><b><u><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'><span
 style='text-decoration:none'>&nbsp;</span></span></u></b></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>The
user grants to AIC ESTONIA full authorization to review the documents and
determine if any habilitation, limitation, suspension or closure of an user`s
account is required. The user also grants to AIC ESTONIA the full authorization
to send any submitted information and/or documents to be verified or used with
legal purposes by any requiring governmental institution, in case of suspicious
activities or other user conducts. AIC ESTONIA will report to any jurisdictional
authorities, either local or foreign, in good faith, any such transactions
which may be deemed obscure or in any way doubtful of the operationfs
destination; this extends whenever required by applicable law due to the amount
of a transaction, according to local and international laws. AIC ESTONIA may
also record all communications, IP addresses, online activity on the AIC ESTONIA
Website, and furthermore all transactions made by its users. Users will agree
and authorize AIC ESTONIA to use this information, for compliance of these
Policies, in any way AIC ESTONIA deems necessary at their own sole discretion,
and to the maximum extent permitted by law. </span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-bottom:0mm;margin-bottom:.0001pt;
text-align:justify;text-justify:inter-ideograph;text-indent:-18.0pt;background:
white'><b><span lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;
color:#0F0F0F'>E.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></b><b><u><span
lang=EN-US style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>Compliance
with AML/CFT by AIC ESTONIA.</span></u></b></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>AIC ESTONIA
will procure and assign a particular specialist for the sole purpose of
verification of these Policies, and the compliance of all AML/CFT regulations
on a local and international level. This person will be familiarized with these
Policies and may change them only to improve and make these Policies more law
compliant by both AIC ESTONIA and its users. The person in charge of compliance
of these policies will be reachable at [EMAIL ADDRESS] and will respond, making
no official representations or statements on behalf of AIC ESTONIA, but may
notify AIC ESTONIA in case this should be required by a governmental instance or
generally by any applicable laws. Users may also inquire about these Policies
to this person.</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0mm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;background:white'><span lang=EN-US
style='font-size:12.0pt;font-family:"Raleway",sans-serif;color:#0F0F0F'>December,
2015. </span></p>



							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<div id="sample" class="hide">
    <p>sample</p>
</div>

		

		

	</div>

	<footer class="site-footer">

		<div class="container">

			<div class="row">

				<div class="col-md-12">

					<div class="footer-logo">
						<a href="#"><img src="img/degicoin_logo.png" alt=""></a>
					</div>

					<div class="text-center">
					<a href="#"><small>Online selling agreement</small></a>　　
					<a href="https://aicestonia.ee/privacy-policy.php"><small>Privacy policy</small></a>　　
					<a href="https://aicestonia.ee/sctl.php"><small>Specific Transactions</small></a>　　
					</div>

					<div class="text-center">
					<small>Copyright aicestonia.ee</small>
					</div>

					<div class="social-list">
						<ul>
							<!-- List of social icons here -->
							<li><a href="#"><i class="{social icon class here}"></i></a></li>
						</ul>
					</div>

				</div>

			</div>

		</div>

	</footer>

</div>

<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="assets/js/hydrogen.setup.min.js"></script>
<script type="text/javascript" src="assets/js/hydrogen.scripts.min.js"></script>
<script type="text/javascript" src="assets/js/hydrogen.plugins.js"></script>

<script type="text/javascript" src="assets/plugins/royalslider/jquery.royalslider.min.js"></script>
<script type="text/javascript" src="assets/plugins/mfp/jquery.mfp-0.9.9.js"></script>
<script type="text/javascript" src="assets/plugins/mediaelement/mediaelement-and-player.min.js"></script>
<script type="text/javascript" src="assets/plugins/gmap/gmap3.min.js"></script>
<script type="text/javascript" src="assets/plugins/owlcarousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/plugins/isotope/jquery.isotope.min.js"></script>
<script type="text/javascript" src="assets/plugins/form/jquery.form.min.js"></script>
<script type="text/javascript" src="assets/plugins/form/jquery.validate.min.js"></script>

<script type="text/javascript" src="assets/js/jquery.cookieBar.js"></script>


<script>
$(function() {
	$.cookieBar({
		style: 'bottom'
		});
});
</script>
</body>

</html>